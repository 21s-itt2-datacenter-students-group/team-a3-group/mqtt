import paho.mqtt.client as mqtt 
from random import randrange
import time

mqttBroker ="broker.hivemq.com" 

client = mqtt.Client("temp")
client.connect(mqttBroker) 

while True:
    client.publish("temp", 'Fill in temperature')
    print("Just published temperature to topic temp")
    time.sleep(1)
